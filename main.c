#include <stdio.h>
#include <stdlib.h>

int main(){
    int a;
    printf("Enter number: ");
    scanf("%d", &a);
    triangle(a);
}

void triangle(int num){
    for(int i=1;i<=num;i++){
        print(i);
        printf("\n");
    }
}

int print(int num){
    if(num>=1){
        printf("%d ",num);
        print(num-1);
    }
}

